import socket
import time

import libnfs
import ovmclient
from ovmclient import constants
import paramiko

from coriolis_cd import coriolis_cd_constants
from coriolis_cd import schemas
from coriolis_cd.providers import base
from coriolis_cd.utils import _log_msg

# template_name = 'coriolis-demo-base'
# vm_name = 'cloudbase-demo'
# assembly_name = "coriolis-demo"

# vm_username = "root"
# vm_password = "coriolis"

_CONN_INFO_SCHEMA = schemas.get_schema(
    __name__, schemas._CONN_INFO_SCHEMA_NAME)
_ENV_INFO_SCHEMA = schemas.get_schema(
    __name__, schemas._ENV_INFO_SCHEMA_NAME)


def _get_ovm_client(host, username, password):
    client = ovmclient.Client(
        'https://%s:7002/ovm/core/wsapi/rest' % host, username, password)
    client.managers.wait_for_manager_state()
    return client


def _clone_template(client, pool_name, template_name, vm_name):
    pool_id = client.server_pools.get_id_by_name(pool_name)
    template_vm = client.vms.get_by_name(template_name)

    print('Cloning template "%s"' % template_name)
    job = client.jobs.wait_for_job(client.vms.clone(
        template_vm['id'], pool_id))
    vm_id = job['resultId']

    try:
        data = client.vms.get_by_id(vm_id)
        data["name"] = vm_name
        client.jobs.wait_for_job(client.vms.update(vm_id, data))

        print('Starting VM "%s"' % vm_name)
        client.jobs.wait_for_job(client.vms.start(vm_id))

        print('Waiting for IP address')

        ip = None
        while not ip:
            vnic = client.vm_virtual_nics(vm_id).get_all()[0]
            ips = [i for i in vnic['ipAddresses'] if i['type'] == 'IPV4']
            if (ips and ips[0].get('address') and
                    ips[0].get('address') != '0.0.0.0'):
                ip = ips[0]['address']
            else:
                time.sleep(1)

        return (vm_id['value'], ip)
    except BaseException:
        if vm_id:
            _delete_vm(client, vm_id)
        raise


def _delete_vm(client, vm_id):
    try:
        print('Killing VM "%s"' % vm_id)
        client.jobs.wait_for_job(client.vms.kill(vm_id))
    except Exception as ex:
        # Ignore
        print(ex)

    vm = client.vms.get_by_id(vm_id)

    print('Deleting VM "%s"' % vm_id)
    client.vms.delete(vm_id)

    for disk_mapping in vm['vmDiskMappingIds']:
        disk_id = client.disk_mappings.get_by_id(
            disk_mapping["value"])["virtualDiskId"]["value"]
        print("Deleting virtual disk \"%s\"" % disk_id)
        client.virtual_disks.delete(disk_id)


def _wait_for_vm_to_stop(client, vm_id, retry_times=30, retry_period=10):
    i = 0
    while True:
        if i == retry_times:
            raise Exception(
                "Reached max retries while waiting for VM '%s' to stop" % (
                    vm_id))

        vm = client.vms.get_by_id(vm_id)
        if vm['vmRunState'] == 'STOPPED':
            break
        else:
            time.sleep(retry_period)
        i = i + 1


def _export_vm_appliance(client, vm_id, repo_name, assembly_name):
    repo_id = client.repositories.get_id_by_name(repo_name)
    # simpleId format is needed
    vm = client.vms.get_by_id(vm_id)
    vm_id = vm["id"]

    job = client.jobs.wait_for_job(
        client.repositories.export_as_assembly(
            repo_id, assembly_name, [vm_id]))
    return job["resultId"]


def _get_local_ip_address(server_ip):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect((server_ip, 22))
    return s.getsockname()[0]


def _create_repository_export(client, pool_name, repo_name):
    repository_id = client.repositories.get_id_by_name(repo_name)
    pool_id = client.server_pools.get_id_by_name(pool_name)
    server = client.server_pool_servers(pool_id).get_all()[0]
    local_ip = _get_local_ip_address(server["ipAddress"])
    nfs_options = "ro, async, no_root_squash"

    repo_exports_mgr = client.server_repository_exports(server["id"])

    # Check if the export already exists first
    for repo_exp in repo_exports_mgr.get_all():
        if (repo_exp["repositoryId"] == repository_id and
                repo_exp["clientHostname"] == local_ip):
            if repo_exp["options"] != nfs_options:
                repo_exports_mgr.delete(repo_exp["id"])
            else:
                return repo_exp["id"]

    data = {}
    data["exportFsType"] = constants.EXPORT_FS_TYPE_NFS
    data["serverId"] = server["id"]
    data["repositoryId"] = repository_id
    data["clientHostname"] = local_ip
    data["options"] = nfs_options

    job = client.jobs.wait_for_job(repo_exports_mgr.create(data))
    return job["resultId"]


def _get_nfs_url(client, repo_export_id):
    repo_export = client.repository_exports.get_by_id(repo_export_id)
    path = repo_export["repositoryPath"]
    server = client.servers.get_by_id(repo_export['serverId'])

    # Connect using root as uid/gid
    return "nfs://%s%s?uid=0&gid=0" % (server["ipAddress"], path)


def _copy_ova(nfs_url, assembly_id, target_path):
    nfs = libnfs.NFS(str(nfs_url))
    package_path = 'Assemblies/%s/package.ova' % assembly_id["value"]
    fr = nfs.open(str(package_path), mode='rb')
    with open(target_path, 'wb') as fw:
        while True:
            buf = fr.read(10 * 1024 * 1024)
            if not buf:
                break
            fw.write(buf)
    fr.close()


def _delete_assembly(client, repo_name, assembly_id):
    repository_id = client.repositories.get_id_by_name(repo_name)
    client.jobs.wait_for_job(
        client.repository_assemblies(repository_id).delete(assembly_id))


def _get_ssh_client(ip, vm_username, vm_password):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(
        paramiko.AutoAddPolicy())
    ssh.connect(ip, username=vm_username, password=vm_password)
    return ssh


def _exec_cmd(ssh, cmd, check_exit_code=True):
    stdin, stdout, stderr = ssh.exec_command(cmd)
    print(stdout.readlines())
    print(stderr.readlines())
    exit_code = stdout.channel.recv_exit_status()
    if check_exit_code and exit_code != 0:
        raise Exception("Command exited with code: %s" % exit_code)


class ApplianceTemplateBootProvider(base.BaseApplianceTemplateBootProvider):
    """ OVM boot provider """
    platform = coriolis_cd_constants.PLATFORM_OVM
    conn_info_schema = _CONN_INFO_SCHEMA
    env_info_schema = _ENV_INFO_SCHEMA

    def boot_appliance_template(self, conn_info, env_info, vm_identifier):
        host_param = conn_info['host']
        username_param = conn_info['username']
        password_param = conn_info['password']

        template_name_param = env_info['template_name']
        pool_name_param = env_info['pool_name']

        client = _get_ovm_client(host_param, username_param, password_param)
        vm_id, vm_ip = _clone_template(client, pool_name_param,
                                       template_name_param, vm_identifier)

        columns = ("VM IP",)
        data = (vm_ip,)

        return columns, data

    def delete_instance(self, conn_info, env_info, vm_identifier):
        host_param = conn_info['host']
        username_param = conn_info['username']
        password_param = conn_info['password']

        client = _get_ovm_client(host_param, username_param, password_param)
        _delete_vm(client, vm_identifier)

    def boot_appliance_from_url(self, conn_info, env_info, vm_identifier,
                                appliance_url):
        host_param = conn_info['host']
        username_param = conn_info['username']
        password_param = conn_info['password']

        template_name_param = env_info['template_name']
        pool_name_param = env_info['pool_name']

        client = _get_ovm_client(host_param, username_param, password_param)
        # NOTE: not implemented yet

        columns = ("VM IP",)
        data = (vm_ip,)

        return columns, data


class ApplianceExportProvider(base.BaseApplianceExportProvider):
    """ OVM export provider """
    platform = coriolis_cd_constants.PLATFORM_OVM
    conn_info_schema = _CONN_INFO_SCHEMA
    env_info_schema = _ENV_INFO_SCHEMA

    def export_appliance(self, conn_info, env_info, vm_identifier,
                         export_path):
        host_param = conn_info['host']
        username_param = conn_info['username']
        password_param = conn_info['password']

        repo_name_param = env_info['repo_name']
        pool_name_param = env_info['pool_name']

        client = _get_ovm_client(host_param, username_param, password_param)
        vm_assembly_name = "%s_assembly" % vm_identifier
        assembly_id = _export_vm_appliance(client, vm_identifier,
                                           repo_name_param, vm_assembly_name)
        repo_export_id = _create_repository_export(client, pool_name_param,
                                                   repo_name_param)
        nfs_url = _get_nfs_url(client, repo_export_id)
        _copy_ova(nfs_url, assembly_id, export_path)

        columns = ('VM exported to',)
        data = (export_dir,)

        return columns, data
