# Copyright 2018 Cloudbase Solutions Srl
# All Rights Reserved.

import contextlib
import os
import ssl
import threading
import time
import tarfile
import shutil
from xml.etree import ElementTree

import requests
from urllib import request
from threading import Thread

from pyVim import connect as pyVconnect
from pyVmomi import vim

from coriolis_cd import constants
from coriolis_cd import schemas
from coriolis_cd import utils
from coriolis_cd.providers import base

PLATFORM_VMWARE = constants.PLATFORM_VMWARE
_CONN_INFO_SCHEMA = schemas.get_schema(
    __name__, schemas._CONN_INFO_SCHEMA_NAME)
_ENV_INFO_SCHEMA = schemas.get_schema(
    __name__, schemas._ENV_INFO_SCHEMA_NAME)
_VM_CONFIG_SCHEMA = schemas.get_schema(
    __name__, schemas._VM_CONFIG_SCHEMA_NAME)
_APPLIANCE_CONFIG_SCHEMA = schemas.get_schema(
    __name__, schemas._APPLIANCE_CONFIG_SCHEMA_NAME)


def _keep_alive_vmware_conn(si, exit_event):
    try:
        while True:
            utils._log_msg("VMware connection keep alive")
            si.CurrentTime()
            if exit_event.wait(60):
                return
    finally:
        utils._log_msg("Exiting VMware connection keep alive thread")


@contextlib.contextmanager
def connect(host, username, password, port=443, allow_untrusted=False):
    """ Connects to the specified host and launches a thread
    to keep session alive. """
    context = ssl.SSLContext(ssl.PROTOCOL_SSLv23)
    if allow_untrusted:
        context.verify_mode = ssl.CERT_NONE

    utils._log_msg("Connecting to host: '%s'" % host)
    si = pyVconnect.SmartConnect(
        host=host,
        user=username,
        pwd=password,
        port=port,
        sslContext=context)

    thread = None
    try:
        thread_exit_event = threading.Event()
        thread = threading.Thread(
            target=_keep_alive_vmware_conn,
            args=(si, thread_exit_event))
        thread.start()

        yield context, si
    finally:
        pyVconnect.Disconnect(si)
        if thread:
            thread_exit_event.set()
            thread.join()


def _wait_for_task(task):
    """ Wait for a task to finish """
    while task.info.state not in [
            vim.TaskInfo.State.success, vim.TaskInfo.State.error]:
        time.sleep(1)
    if task.info.state == vim.TaskInfo.State.error:
        raise Exception(task.info.error.msg)


def _get_obj(
        content, vimtype, resource_name=None, error_on_not_found=True):
    """
    param resource_name: str: name of the resource - may be None, in which case
    the first resource of that type in the list is returned.
    param error_on_not_found: bool: whether to raise if not found
    """
    obj = None
    container = content.viewManager.CreateContainerView(
        content.rootFolder, vimtype, True)
    if resource_name:
        for c in container.view:
            if c.name == resource_name:
                obj = c
                break
    else:
        # if no name was given, we just return the first avilable
        # resource of this type:
        allobjs = list(container.view)
        if allobjs:
            return allobjs[0]
        else:
            raise Exception(
                "Could not find any resource of type '%s'" % vimtype)

    if obj is None and error_on_not_found:
        raise Exception(
            "Could not find item with name '%s' and type '%s'." % (
                resource_name, vimtype))

    return obj


def _get_vm(si, vm_name):
    # NOTE: retrieve fresh content:
    content = si.RetrieveContent()
    return _get_obj(
        content, [vim.VirtualMachine],
        resource_name=vm_name,
        error_on_not_found=True)


def _wait_for_vm_status(vmobj, status, period=10, max_wait=180):
    utils._log_msg("Waiting for VM '%s' status '%s'." % (vmobj.name, status))
    i = 0
    while i < max_wait and vmobj.runtime.powerState != status:
        utils._log_msg(
            "Waiting for VM '%s' status '%s'. Current status '%s'" % (
                vmobj.name, status, vmobj.runtime.powerState))
        time.sleep(period)
        i += period

    if i >= max_wait:
        raise Exception(
            "Failed waiting for VM '%s' to reach status '%s'. "
            "Current status is: '%s'" % (
                vmobj.name, status, vmobj.runtime.powerState))


def _get_largest_free_ds(dc):
    """
    Pick the datastore that is accessible with the largest free space.
    """
    largest = None
    largestFree = 0
    for ds in dc.datastore:
        try:
            freeSpace = ds.summary.freeSpace
            if freeSpace > largestFree and ds.summary.accessible:
                largestFree = freeSpace
                largest = ds
        except BaseException:  # Ignore datastores that have issues
            pass
    if largest is None:
        raise Exception('Failed to find any free datastores on %s' % dc.name)
    return largest


def _get_datacenter_datastore(datacenter, datastore_name):
    """
    Finds datastore with passed name, while making sure it resides in the
    passed datacenter.
    """
    for ds in datacenter.datastore:
        if ds.name == datastore_name:
            return ds
    raise Exception("Failed to find datastore '%s' in datacenter '%s'" % (
        datastore_name, datacenter.name))


def _download_file(url):
    local_filename = url.split('/')[-1]
    with requests.get(url, stream=True) as r:
        r.raise_for_status()
        with open(local_filename, 'wb') as f:
            for chunk in r.iter_content(chunk_size=8192):
                if chunk:  # filter out keep-alive new chunks
                    f.write(chunk)
                    # f.flush()
    return local_filename


def _get_tarfile_size(tarfile):
    """
    Determine the size of a file inside a tarball.
    If the object has a size attribute, use that. Otherwise seek to the end
    and report that.
    """
    if hasattr(tarfile, 'size'):
        return tarfile.size
    size = tarfile.seek(0, 2)
    tarfile.seek(0, 0)
    return size


def _keep_lease_alive(lease):
    """
    Keeps the lease alive while deploying disks.
    """
    while(True):
        utils._log_msg("keeping lease alive")
        time.sleep(5)
        try:
            # Choosing arbitrary percentage to keep the lease alive.
            lease.HttpNfcLeaseProgress(50)
            if (lease.state == vim.HttpNfcLease.State.done):
                return
            # If the lease is released, we get an exception.
            # Returning to kill the thread.
        except BaseException:
            return


def _clone_template(si, template_name, dc_name, new_vm_name, start_vm=True):
    if template_name == new_vm_name:
        raise ValueError(
            "'template_name' and 'new_vm_name' must not be equal.")
    content = si.RetrieveContent()

    # retrieve the VM template:
    template = _get_obj(content, [vim.VirtualMachine], template_name)

    # # prepare all cloning params:
    # get DC and root folder of DC:
    datacenter = _get_obj(content, [vim.Datacenter], dc_name)
    folder = datacenter.vmFolder

    # get datastore name off template:
    datastore = _get_obj(
        content, [vim.Datastore], template.datastore[0].info.name)

    # get first available cluster and its default resource pool:
    cluster = _get_obj(
        content, [vim.ClusterComputeResource], resource_name=None)
    resource_pool = cluster.resourcePool

    # set relocation and cloning specs:
    relocspec = vim.vm.RelocateSpec()
    relocspec.datastore = datastore
    relocspec.pool = resource_pool

    clonespec = vim.vm.CloneSpec()
    clonespec.location = relocspec
    clonespec.powerOn = start_vm

    new_vm = None
    try:
        utils._log_msg(
            "Cloning template '%s' as VM '%s'" % (
                template_name, new_vm_name))
        task = template.Clone(folder=folder, name=new_vm_name, spec=clonespec)
        _wait_for_task(task)

        new_vm = _get_vm(si, new_vm_name)
    except (Exception, KeyboardInterrupt):
        _check_delete_vm(si, new_vm_name)
        raise

    desired_state = vim.VirtualMachinePowerState.poweredOff
    if start_vm:
        desired_state = vim.VirtualMachinePowerState.poweredOn

    _wait_for_vm_status(new_vm, desired_state)

    return new_vm


def _deploy_ova(si, appliance_url, dc_name, new_vm_name, host,
                datastore_name=None, network_name=None, start_vm=True):
    content = si.RetrieveContent()

    # get DC and root folder of DC:
    datacenter = _get_obj(content, [vim.Datacenter], dc_name)
    folder = datacenter.vmFolder

    # get the datastore
    if datastore_name:
        datastore = _get_datacenter_datastore(datacenter, datastore_name)
    else:
        datastore = _get_largest_free_ds(datacenter)

    network = None
    if network_name:
        network = _get_obj(
            content, [vim.Network], resource_name=network_name,
            error_on_not_found=False)

    # get first available cluster and its default resource pool:
    cluster = _get_obj(
        content, [vim.ClusterComputeResource], resource_name=None)
    resource_pool = cluster.resourcePool

    ova_path = _download_file(appliance_url)
    tfile = tarfile.open(ova_path)
    ovffilename = list(
        filter(lambda x: x.endswith(".ovf"), tfile.getnames()))[0]
    ovffile = tfile.extractfile(ovffilename)
    descriptor = ovffile.read().decode()

    ovfManager = si.content.ovfManager

    network_mapping = []
    if network:
        root = ElementTree.fromstring(descriptor)
        ns = {"ovf": "http://schemas.dmtf.org/ovf/envelope/1"}
        for net in root.findall(
                './ovf:NetworkSection/ovf:Network[@ovf:name]', ns):
            source_network_name = net.get('{%s}name' % ns['ovf'])
            if source_network_name:
                network_mapping.append(vim.OvfManager.NetworkMapping(
                    name=source_network_name,
                    network=network))

    cisp = vim.OvfManager.CreateImportSpecParams(
        entityName=new_vm_name, networkMapping=network_mapping)
    cisr = ovfManager.CreateImportSpec(
        descriptor, resource_pool, datastore, cisp)

    if len(cisr.error):
        utils._log_msg("The following errors will prevent import of this OVA:")
        for error in cisr.error:
            utils._log_msg("%s" % error)
        raise Exception("Could not create import spec")

    new_vm = None

    lease = resource_pool.ImportVApp(cisr.importSpec, folder)
    while lease.state == vim.HttpNfcLease.State.initializing:
        utils._log_msg("Waiting for lease to be ready...")
        time.sleep(1)
    if lease.state == vim.HttpNfcLease.State.error:
        raise Exception("Lease error: %s" % lease.error)
    if lease.state == vim.HttpNfcLease.State.done:
        raise Exception("Lease state is: %s" % lease.state)

    utils._log_msg("Starting deploy")
    keepalive_thread = Thread(target=_keep_lease_alive, args=(lease,))
    keepalive_thread.start()

    try:
        for fileItem in cisr.fileItem:
            diskfilename = list(
                filter(lambda x: x == fileItem.path, tfile.getnames()))[0]
            diskfile = tfile.extractfile(diskfilename)

            deviceurl = None
            for devurl in lease.info.deviceUrl:
                if devurl.importKey == fileItem.deviceId:
                    deviceurl = devurl
                    break
            durl = deviceurl.url.replace('*', host)

            headers = {'Content-length': _get_tarfile_size(diskfile)}
            if hasattr(ssl, '_create_unverified_context'):
                sslContext = ssl._create_unverified_context()
            else:
                sslContext = None
            req = request.Request(durl, diskfile, headers)
            request.urlopen(req, context=sslContext)

        lease.Complete()
        keepalive_thread.join()
        utils._log_msg("Finished upload successfully")
    except Exception as e:
        utils._log_msg("Lease: %s" % lease.info)
        utils._log_msg("Hit an error in upload: %s" % e)
        lease.Abort()
        keepalive_thread.join()
        _check_delete_vm(si, new_vm_name)
        raise

    new_vm = _get_vm(si, new_vm_name)

    desired_state = vim.VirtualMachinePowerState.poweredOff
    if start_vm:
        _wait_for_vm_status(new_vm, desired_state)
        power_task = new_vm.PowerOnVM_Task()
        _wait_for_task(power_task)
        desired_state = vim.VirtualMachinePowerState.poweredOn

    _wait_for_vm_status(new_vm, desired_state)

    utils._log_msg("Cleaning up local copy of ova at: %s" % ova_path)
    if os.path.isfile(ova_path):
        os.remove(ova_path)

    return new_vm


def _check_vmware_tools_installed(vm_obj, raise_if_not=True):
    installed = (vm_obj.guest.toolsStatus !=
                 vim.vm.GuestInfo.ToolsStatus.toolsNotInstalled)

    if raise_if_not and not installed:
        raise Exception(
            "VMWare tools not installed in VM '%s'" % vm_obj.name)

    return installed


def _check_stop_vm(vm_obj):
    if vm_obj.runtime.powerState != vim.VirtualMachinePowerState.poweredOff:
        utils._log_msg(
            "VM '%s' is in status '%s'. Powering off VM." % (
            vm_obj.name, vm_obj.runtime.powerState))
        if vm_obj.runtime.powerState == vim.VirtualMachinePowerState.poweredOn:
            utils._log_msg("Shutting down guest OS for machine '%s'" % (
                vm_obj.name))
            vm_obj.ShutdownGuest()
        else:
            poweroff_task = vm_obj.PowerOff()
            _wait_for_task(poweroff_task)
        _wait_for_vm_status(vm_obj, vim.VirtualMachinePowerState.poweredOff)
    else:
        utils._log_msg("VM '%s' is already off." % vm_obj.name)


def _wait_for_vm_ip_info(si, vm_name, period=10, timeout=180):
    """ Waits for the guest networking info to become available and
    returns the first IP address of the first NIC of the VM. """
    i = 0
    ip_address = None

    vm_obj = _get_vm(si, vm_name)

    utils._log_msg("Waiting for IP address info for VM '%s'." % vm_name)
    while i < timeout:
        vmware_tools_installed = _check_vmware_tools_installed(
            vm_obj, raise_if_not=False)
        if not vmware_tools_installed:
            utils._log_msg(
                "VMWare tools not installed in VM '%s'" % vm_obj.name)
        else:
            nics = list(vm_obj.guest.net)
            if nics:
                nic = nics[0]
                ip_addresses = list(nic.ipAddress)
                ipv4_addresses = [
                    ip for ip in ip_addresses
                    if utils.validate_ipv4_address(ip)]
                if ipv4_addresses:
                    utils._log_msg(
                        "IPv4 addresses for NIC with MAC '%s': '%s'" % (
                            nic.macAddress, ipv4_addresses))
                    ip_address = ipv4_addresses[0]
                    break

        i = i + period
        time.sleep(period)
        vm_obj = _get_vm(si, vm_name)

    if not ip_address:
        raise Exception(
            "Timed out after waiting %d seconds for IP address info "
            "for VM '%s'" % (timeout, vm_name))

    utils._log_msg("Using VM IP address '%s'" % ip_address)
    return ip_address


def _check_delete_vm(si, vm_name):
    # NOTE: retrieve fresh content:
    if not vm_name:
        raise Exception("cannot delete VM '%s' due to invalid identifier")
    content = si.RetrieveContent()
    vm = _get_obj(
        content, [vim.VirtualMachine],
        resource_name=vm_name,
        error_on_not_found=False)

    if vm is None:
        utils._log_msg("Could not locate VM '%s' for deletion; skipping." % (
            vm_name))
        return

    _check_stop_vm(vm)

    utils._log_msg("Destroying VM '%s'" % vm_name)
    destroy_task = vm.Destroy_Task()
    _wait_for_task(destroy_task)

def _get_vm_extraconfig_value(si, vm_name, vm_key):
    """Obtain VM extraconfig option value for a specific key."""
    vm = _get_vm(si, vm_name)
    xtracfg = vm.config.extraConfig
    for opts in xtracfg:
        if opts.key == vm_key:
            if opts.value is not None:
                return opts.value
            else:
                utils._log_msg(
                    "Could not locate key '%s' in VM '%s' extraConfig." %
                    (vm_key, vm_name))

def _set_vm_extra_config_value(si, vm_name, extra_config_key, extra_config_value):
    """Sets a specific key from VM extraconfig.
    A null value will delete the key.
    """
    vm = _get_vm(si, vm_name)
    spec = vim.vm.ConfigSpec()
    spec.extraConfig = []
    opt = vim.option.OptionValue()
    opt.key = extra_config_key
    opt.value = extra_config_value
    utils._log_msg(
        "Setting extraconfig option '%s' with value '%s' on VM '%s'" %
        (vm_name, opt.key, opt.value))
    spec.extraConfig.append(opt)
    task = vm.ReconfigVM_Task(spec)
    _wait_for_task(task)

def _export_vm_appliance(si, context, hostname, vm_name, export_dir):
    """ Exports the contents of the VM in the given existing export dir.
    A '<vm_name>.ova' file will be placed there.

    Returns the full path of the ova file.
    """
    vm = _get_vm(si, vm_name)
    _check_stop_vm(vm)

    #remove nvram option from VM config to allow import on VSphere < 6.7
    #known issue https://kb.vmware.com/s/article/67724
    nvram_file = _get_vm_extraconfig_value(si, vm_name, vm_key="nvram")
    if nvram_file:
        _set_vm_extra_config_value(
            si, vm_name, extra_config_key="nvram", extra_config_value="")

    ovf_files = []
    lease = vm.ExportVm()
    # download all the disks, skip nvram file to allow import on VSphere < 6.7:
    while True:
        if lease.state == vim.HttpNfcLease.State.ready:
            du = None
            try:
                tot_downloaded_bytes = 0
                for du in [du for du in lease.info.deviceUrl if (du.disk and
                        (du.url.split('/')[-1].split('.')[-1] != 'nvram'))]:
                    size_bytes_dev = 0
                    # Key format: '/vm-70/VirtualLsiLogicController0:0'
                    ctrl_str, _ = du.key[
                        du.key.rindex('/') + 1:].split(':')

                    def _get_class_name(obj):
                        return obj.__class__.__name__.split('.')[-1]

                    for i, ctrl in enumerate(
                        [d for d in vm.config.hardware.device if
                         isinstance(
                            d, vim.vm.device.VirtualController) and
                            ctrl_str.startswith(_get_class_name(d))]):
                        if int(ctrl_str[len(_get_class_name(ctrl)):]) == i:
                            break

                    # NOTE: when exporting directly from an ESXi host,
                    # the hostname in the URL will be '*'
                    disk_url = du.url.replace("*", hostname)
                    response = request.urlopen(disk_url, context=context)
                    path = os.path.join(export_dir, du.targetId)

                    utils._log_msg("Downloading disk to path '%s'" % path)
                    with open(path, 'wb') as f:
                        while True:
                            chunk = response.read(1024 * 1024)
                            if not chunk:
                                break
                            tot_downloaded_bytes += len(chunk)
                            size_bytes_dev += len(chunk)
                            f.write(chunk)
                            lease_progress = int(
                                tot_downloaded_bytes * 100 / (
                                    lease.info.totalDiskCapacityInKB * 1024))
                            utils._log_msg("Lease progress: %d%%" % (
                                lease_progress))
                            lease.HttpNfcLeaseProgress(lease_progress)

                    ovf_file = vim.OvfManager.OvfFile()
                    ovf_file.deviceId = du.key
                    # NOTE: path must be relative from root of the OVA dir:
                    ovf_file.path = du.targetId
                    ovf_file.size = size_bytes_dev
                    ovf_files.append(ovf_file)

                lease.HttpNfcLeaseComplete()
                break
            except (Exception, KeyboardInterrupt) as ex:
                disk_name = "unknown"
                if du is not None:
                    disk_name = du.url
                utils._log_msg(
                    "Exception ocurred for disk '%s'. Relinquishing lease."
                    " Exception: %s" % (disk_name, str(ex)))
                lease.HttpNfcLeaseAbort()
                raise
        elif lease.state == vim.HttpNfcLease.State.error:
            raise Exception(lease.error.msg)
        else:
            utils._log_msg("Current export lease state is: '%s'" % lease.state)
            time.sleep(.1)

    # create OVF metafile:
    utils._log_msg("Creating OVF metafile for VM.")
    ovf_descriptor_name = vm.name
    ovf_params = vim.OvfManager.CreateDescriptorParams()
    ovf_params.name = ovf_descriptor_name
    ovf_params.ovfFiles = ovf_files

    ovf_manager = si.content.ovfManager
    ovf_descriptor = ovf_manager.CreateDescriptor(
        obj=vm, cdp=ovf_params)

    if ovf_descriptor.error:
        raise Exception(ovf_descriptor.error[0].fault)
    else:
        # write descriptor file:
        ovf_descriptor_path = "%s.ovf" % export_dir
        ovf_descriptor_data = ovf_descriptor.ovfDescriptor
        with open(ovf_descriptor_path, 'wb') as fout:
            fout.write(ovf_descriptor_data.encode('utf-8'))

    utils._log_msg("Creating OVA file")
    ova_path = "%s.ova" % export_dir
    with tarfile.open(ova_path, "w") as tar:
        # add OVF file at the beginning of the archive
        tar.add(
                ovf_descriptor_path,
                arcname=os.path.basename(ovf_descriptor_path))
        for dir_file in os.listdir(export_dir):
            dir_file_path = os.path.join(export_dir, dir_file)
            tar.add(dir_file_path, arcname=os.path.basename(dir_file_path))

    # move ovf file to export dir to clean up everything at once
    shutil.move(ovf_descriptor_path, export_dir)
    try:
        shutil.rmtree(export_dir)
    except OSError as e:
        utils._log_msg("Error: %s - %s." % (e.filename, e.strerror))
    #add previously removed nvram option in VM config
    if nvram_file:
        _set_vm_extra_config_value(
            si, vm_name, extra_config_key="nvram",
            extra_config_value=nvram_file)
    return os.path.abspath(ova_path)


class ApplianceTemplateBootProvider(base.BaseApplianceTemplateBootProvider):
    """ VMWare boot provider """
    platform = PLATFORM_VMWARE
    conn_info_schema = _CONN_INFO_SCHEMA
    env_info_schema = _ENV_INFO_SCHEMA

    def boot_appliance_template(self, conn_info, env_info, vm_identifier):
        host_param = conn_info['host']
        username_param = conn_info['username']
        password_param = conn_info['password']
        port_param = conn_info['port']
        allow_untrusted_param = conn_info['allow_untrusted']

        template_name_param = env_info['template_name']
        datacenter_name_param = env_info['datacenter_name']

        new_vm = None
        with connect(
                host_param, username_param, password_param, port=port_param,
                allow_untrusted=allow_untrusted_param) as (context, si):
            try:
                new_vm = _clone_template(si, template_name_param,  # noqa: F841
                                         datacenter_name_param, vm_identifier,
                                         start_vm=True)
                vm_ip = _wait_for_vm_ip_info(si, vm_identifier)
            except BaseException:
                utils._log_msg(
                    "Exception occured while trying to boot VM %s. Exception "
                    "details: \n%s" % (
                        vm_identifier, utils.get_exception_details()))
                raise

        columns = ("VM IP",)
        data = (vm_ip,)

        return columns, data

    def delete_instance(self, conn_info, env_info, vm_identifier):
        host_param = conn_info['host']
        username_param = conn_info['username']
        password_param = conn_info['password']
        port_param = conn_info['port']
        allow_untrusted_param = conn_info['allow_untrusted']

        with connect(
                host_param, username_param, password_param, port=port_param,
                allow_untrusted=allow_untrusted_param) as (context, si):
            try:
                _check_delete_vm(si, vm_identifier)
            except BaseException:
                utils._log_msg(
                    "Exception occured while deleting instance %s. Exception "
                    "details:\n%s" % (vm_identifier,
                                      utils.get_exception_details()))
                raise

        columns = ("VM Name", "State")
        data = (vm_identifier, "deleted")

        return columns, data

    def boot_appliance_from_url(self, conn_info, env_info, vm_identifier,
                                appliance_url):
        host_param = conn_info['host']
        username_param = conn_info['username']
        password_param = conn_info['password']
        port_param = conn_info['port']
        allow_untrusted_param = conn_info['allow_untrusted']

        datacenter_name_param = env_info['datacenter_name']
        datastore_name = env_info.get('datastore_name')
        network_name = env_info.get('network_name')

        new_vm = None
        with connect(
                host_param, username_param, password_param, port=port_param,
                allow_untrusted=allow_untrusted_param) as (context, si):
            try:
                new_vm = _deploy_ova(si, appliance_url, datacenter_name_param,
                                     vm_identifier, host_param,
                                     datastore_name=datastore_name,
                                     network_name=network_name,
                                     start_vm=True)
                vm_ip = _wait_for_vm_ip_info(si, vm_identifier)
            except BaseException:
                utils._log_msg(
                    "Exception occured while deploying VM from url %s. "
                    "Exception details: \n%s" % (appliance_url,
                                                 utils.get_exception_details()))
                raise

        columns = ("VM IP",)
        data = (vm_ip,)

        return columns, data


class ApplianceExportProvider(base.BaseApplianceExportProvider):
    """ VMWare export provider """
    platform = PLATFORM_VMWARE
    conn_info_schema = _CONN_INFO_SCHEMA
    env_info_schema = _ENV_INFO_SCHEMA

    def export_appliance(self, conn_info, env_info, vm_identifier,
                         export_path):
        host_param = conn_info['host']
        username_param = conn_info['username']
        password_param = conn_info['password']
        port_param = conn_info['port']
        allow_untrusted_param = conn_info['allow_untrusted']
        utils._log_msg("exporting VM")

        export_dir = os.path.join(export_path, vm_identifier.replace(" ", ""))
        if not os.path.exists(export_dir) and os.path.exists(export_path):
            os.mkdir(export_dir)
        if not os.path.isdir(export_dir) or os.listdir(export_dir):
            raise Exception(
                "The export directory '%s' must exist and be empty." %
                export_dir)

        ova_file_path = None
        with connect(
                host_param, username_param, password_param, port=port_param,
                allow_untrusted=allow_untrusted_param) as (context, si):
            try:
                vm = _get_vm(si, vm_identifier)
                _check_stop_vm(vm)
                ova_file_path = _export_vm_appliance(  # noqa: F841
                    si, context, host_param, vm.name, export_dir)
            except BaseException:
                utils._log_msg(
                    "Exception occured while exporting VM %s. Exception "
                    "details:\n%s" % (vm_identifier,
                                      utils.get_exception_details()))
                raise

        columns = ('VM exported to',)
        data = (ova_file_path,)

        return columns, data


class BareMetalProvider(base.BaseBareMetalProvider):
    """ VMWare Bare Metal provider """
    platform = PLATFORM_VMWARE
    conn_info_schema = _CONN_INFO_SCHEMA
    env_info_schema = _ENV_INFO_SCHEMA
    vm_config_schema = _VM_CONFIG_SCHEMA
    appliance_config_schema = _APPLIANCE_CONFIG_SCHEMA

    def deploy_bare_metal_machine(self, conn_info, vm_config, appliance_config):
        host_param = conn_info['host']
        username_param = conn_info['username']
        password_param = conn_info['password']
        port_param = conn_info['port']
        allow_untrusted_param = conn_info['allow_untrusted']

        template_name_param = vm_config['template_name']
        vm_identifier = vm_config['vm_name']
        datacenter_name_param = vm_config['datacenter_name']
        vm_username_param = vm_config['username']
        vm_password_param = vm_config['password']
        vm_agent_url = vm_config['agent_url']
        vm_temp_dir = "/tmp/snapshot-agent"

        appliance_ip_param  = appliance_config['host']
        appliance_username_param = appliance_config['user']
        appliance_password_param = appliance_config['password']
        agent_exec_path = "/tmp/snapshot-agent/coriolis-snapshot-agent"

        new_vm = None
        with connect(
                host_param, username_param, password_param, port=port_param,
                allow_untrusted=allow_untrusted_param) as (context, si):
            try:
                new_vm = _clone_template(si, template_name_param,  # noqa: F841
                                         datacenter_name_param, vm_identifier,
                                         start_vm=True)
                vm_ip = _wait_for_vm_ip_info(si, vm_identifier)
            except BaseException:
                utils._log_msg(
                    "Exception occured while trying to boot VM %s. Exception "
                    "details: \n%s" % (
                        vm_identifier, utils.get_exception_details()))
                raise

        agent_file = _download_file(vm_agent_url)
        remote_file = os.path.join(vm_temp_dir, agent_file)
        vm_ssh = utils._get_ssh_client(vm_ip, vm_username_param,
                                       vm_password_param)
        utils._exec_ssh_cmd(
            vm_ssh, "sudo /usr/bin/hostnamectl set-hostname %s" %
            vm_identifier)
        utils._exec_ssh_cmd(vm_ssh, "mkdir -p %s" % vm_temp_dir)
        utils._send_file(vm_ssh, agent_file, remote_file)
        utils._exec_ssh_cmd(
            vm_ssh, "tar -xvf %s -C %s --strip-components=2" % (
                remote_file, vm_temp_dir))
        # temporarily workaround until we can pass arguments to snapshot binary
        agent_sh = _download_file(
            "https://raw.githubusercontent.com/gluka-CBS/"
            "coriolis-snapshot-agent/automm_install/scripts/setup/setup.sh")
        remote_sh = os.path.join(vm_temp_dir, agent_sh)
        utils._send_file(vm_ssh, agent_sh, remote_sh)
        utils._exec_ssh_cmd(vm_ssh, "chmod +x %s" % remote_sh)
        # we need the service and config sample file for setup.sh to work
        service_sample = _download_file(
            "https://raw.githubusercontent.com/cloudbase/"
            "coriolis-snapshot-agent/main/scripts/setup/"
            "coriolis-snapshot-agent.service.sample")
        remote_service_sample = os.path.join(vm_temp_dir, service_sample)
        utils._send_file(vm_ssh, service_sample, remote_service_sample)
        config_sample= _download_file(
            "https://raw.githubusercontent.com/gluka-CBS/"
            "coriolis-snapshot-agent/main/scripts/setup/config-template.toml")
        remote_service_sample = os.path.join(vm_temp_dir, config_sample)
        utils._send_file(vm_ssh, config_sample, remote_service_sample)
        # workaround ends here

        appliance_ssh =  utils._get_ssh_client(appliance_ip_param,
                                              appliance_username_param,
                                              appliance_password_param)
        fingerprint = utils.get_appliance_fingerprint(appliance_ssh)
        ssh_cmd = "sudo -E %s -H %s -f %s -e %s" % (
            remote_sh, appliance_ip_param, fingerprint, agent_exec_path)
        utils._exec_ssh_cmd(vm_ssh, ssh_cmd)

        columns = ('VM IP',)
        data = (vm_ip,)

        return columns, data

    def bare_metal_machine_register(self, conn_info, vm_config,
                                    appliance_config):
        """ Improvement: Use coriolis-metal-hub client for BM registration """
        host_param = conn_info['host']
        username_param = conn_info['username']
        password_param = conn_info['password']
        port_param = conn_info['port']
        allow_untrusted_param = conn_info['allow_untrusted']

        vm_identifier = vm_config['vm_name']

        appliance_ip_param  = appliance_config['host']
        appliance_username_param = appliance_config['user']
        appliance_password_param = appliance_config['password']

        with connect(
                host_param, username_param, password_param, port=port_param,
                allow_untrusted=allow_untrusted_param) as (context, si):
            try:
                vm_ip = _wait_for_vm_ip_info(si, vm_identifier)
            except BaseException:
                utils._log_msg(
                    "Exception occured while trying to boot VM %s. Exception "
                    "details: \n%s" % (
                        vm_identifier, utils.get_exception_details()))
                raise

        appliance_ssh =  utils._get_ssh_client(appliance_ip_param,
                                              appliance_username_param,
                                              appliance_password_param)
        metal_endpoint = "http://%s:9900/api/v1" % appliance_ip_param
        bm_endpoint = "https://%s:9999/api/v1" % vm_ip
        ssh_cmd = (
            "coriolis-metal-hub --endpoint %s server add %s -f json | "
            "jq '.ID'" % (metal_endpoint, bm_endpoint))
        bm_id = utils._exec_ssh_cmd_sourced(appliance_ssh, ssh_cmd).rstrip('\n')
        refresh_cmd = (
            "coriolis-metal-hub --endpoint %s server refresh %s " % (
                metal_endpoint, bm_id))
        utils._exec_ssh_cmd_sourced(appliance_ssh, refresh_cmd)

        columns = ('BM ID',)
        data = (bm_id,)
        return columns, data

    def bare_metal_machine_delete(self, conn_info, vm_config):
        """ Deletes VM from hypervisor """
        host_param = conn_info['host']
        username_param = conn_info['username']
        password_param = conn_info['password']
        port_param = conn_info['port']
        allow_untrusted_param = conn_info['allow_untrusted']
        vm_identifier = vm_config['vm_name']

        with connect(
                host_param, username_param, password_param, port=port_param,
                allow_untrusted=allow_untrusted_param) as (context, si):
            try:
                _check_delete_vm(si, vm_identifier)
            except BaseException:
                utils._log_msg(
                    "Exception occured while deleting instance %s. Exception "
                    "details:\n%s" % (vm_identifier,
                                      utils.get_exception_details()))
                raise

        columns = ("VM Name", "State")
        data = (vm_identifier, "deleted")

        return columns, data
