# Copyright 2019 Cloudbase Solutions Srl
# All Rights Reserved.

"""Defines various schemas used for validation throughout the project."""

import json

import jinja2
import jsonschema

from coriolis_cd import utils

DEFAULT_SCHEMAS_DIRECTORY = "schemas"

_CONN_INFO_SCHEMA_NAME = "conn_info_schema.json"
_ENV_INFO_SCHEMA_NAME = "env_info_schema.json"
_TESTING_CONFIG_SCHEMA_NAME = "testing_config.json"
_VM_CONFIG_SCHEMA_NAME = "vm_config_schema.json"
_APPLIANCE_CONFIG_SCHEMA_NAME = "appliance_config_schema.json"


def get_schema(package_name, schema_name,
               schemas_directory=DEFAULT_SCHEMAS_DIRECTORY):
    """Loads the schema using jinja2 template loading.
    Loads the schema with the given 'schema_name' using jinja2 template
    loading from the provided 'package_name' under the given
    'schemas_directory'.
    """
    template_env = jinja2.Environment(
        loader=jinja2.PackageLoader(package_name, schemas_directory))

    schema = json.loads(template_env.get_template(schema_name).render())

    utils._log_msg("Succesfully loaded and parsed schema '%s' from '%s'." % (
                   schema_name, package_name))
    return schema


def validate_value(val, schema, format_checker=None):
    """Simple wrapper for jsonschema.validate for usability.
    NOTE: silently passes empty schemas.
    """
    try:
        jsonschema.validate(val, schema, format_checker=format_checker)
    except jsonschema.exceptions.ValidationError as ex:
        utils._log_msg("Exception occured while validating schema:\n%s" % (
            utils.get_exception_details()))
        raise Exception("Schema validation failed: %s" % str(ex))


def validate_string(json_string, schema):
    """Attempts to validate the given json string against the JSON schema.
    Runs silently on success or raises an exception otherwise.
    Silently passes empty schemas.
    """
    validate_value(json.loads(json_string), schema)
