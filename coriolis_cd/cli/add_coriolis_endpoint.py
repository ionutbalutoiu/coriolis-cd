# Copyright 2019 Cloudbase Solutions Srl
# All Rights Reserved.

from cliff.command import Command

from coriolis_cd.operations import coriolis_endpoint


class AddCoriolisEndpoint(Command):
    """Add an endpoint to a coriolis appliance"""

    def get_parser(self, prog_name):
        parser = super(AddCoriolisEndpoint, self).get_parser(prog_name)
        parser.add_argument("--name",
                            type=str,
                            help="Endpoint name",
                            required=True)
        parser.add_argument("--description",
                            type=str,
                            help="Endpoint description",
                            required=True)
        parser.add_argument("--type",
                            type=str,
                            help="Endpoint type",
                            required=True)
        parser.add_argument("--connection-info",
                            type=str,
                            help="Connection info in JSON format",
                            required=True)
        parser.add_argument("--auth-url",
                            type=str,
                            help="Coriolis Keystone auth-url",
                            required=True)
        parser.add_argument("--user-name",
                            type=str,
                            help="Coriolis Keystone username",
                            required=True)
        parser.add_argument("--user-password",
                            type=str,
                            help="Coriolis Keystone user password",
                            required=True)
        parser.add_argument("--project-name", default="admin",
                            type=str,
                            help="Coriolis Keystone project name")
        parser.add_argument("--user-domain-name", default="Default",
                            type=str,
                            help="Coriolis Keystone user domain name")
        parser.add_argument("--project-domain-name", default="Default",
                            type=str,
                            help="Coriolis Keystone project domain name")
        return parser

    def take_action(self, args):
        coriolis_endpoint.add_and_validate_endpoint(args.auth_url,
                                                    args.user_name,
                                                    args.user_password,
                                                    args.project_name,
                                                    args.user_domain_name,
                                                    args.project_domain_name,
                                                    args.name,
                                                    args.description,
                                                    args.type,
                                                    args.connection_info)
