# Copyright 2019 Cloudbase Solutions Srl
# All Rights Reserved.

import json

from cliff.command import Command

from coriolis_cd import utils as coriolis_cd_utils
from coriolis_cd.operations import coriolis_build


class Build(Command):
    """Build coriolis components on an existing VM via SSH"""

    def get_parser(self, prog_name):
        parser = super(Build, self).get_parser(prog_name)
        parser.add_argument('--host', dest="host", required=True,
                            help='Hostname/address of the VM')
        parser.add_argument('--username', dest="username", required=True,
                            help='VM username.')
        parser.add_argument('--password', dest="password",
                            help='VM password', required=True)
        parser.add_argument('--key-path', dest="key_path",
                            help='Path to bitbucket key', required=True)
        parser.add_argument('--remote-key-path', dest="remote_key_path",
                            required=True,
                            help='Path to upload the bitbucket key to')
        parser.add_argument('--docker-registry', dest="registry_host",
                            help='Docker registry', required=True)
        parser.add_argument('--docker-registry-user', dest="registry_username",
                            help='Docker registry user', required=True)
        parser.add_argument('--docker-registry-password',
                            dest="registry_password",
                            help='Docker registry user', required=True)
        parser.add_argument('--export-providers', required=True,
                            dest="export_providers", default=[], nargs='+',
                            help='List of export providers to be added to '
                            'the image')
        parser.add_argument('--import-providers', required=True,
                            dest="import_providers", default=[], nargs='+',
                            help='List of import providers to be added to '
                            'the image')
        parser.add_argument('--provider-custom-repo-names', type=str,
                            dest="custom_repo_names", required=False,
                            help='Dictionary containing provider name - repo '
                            'username mappings. Please use the following '
                            'format (double quotes included): "{openstack: '
                            'cloudbase, aws: cloudengineer, etc: etc"')
        parser.add_argument('--provider-custom-branch-names', type=str,
                            dest="custom_branch_names", required=False,
                            help='Dictionary containing provider name - '
                            'branch name mappings. Please use the following '
                            'format (double quotes included): "{openstack: '
                            'master, aws: branch1, etc: etc"')
        parser.add_argument('--coriolis-docker-repo-url',
                            dest="coriolis_docker_repo_url",
                            required=False, default="https://bitbucket.org/"
                            "cloudbase/coriolis-docker.git",
                            help='The URL used to clone coriolis-docker '
                            'git repository')
        parser.add_argument('--coriolis-docker-branch-name',
                            dest="coriolis_docker_branch_name",
                            required=False, default="master",
                            help='The branch name used to clone '
                            'coriolis-docker git repository')
        return parser

    def take_action(self, args):
        custom_repos = {}
        if args.custom_repo_names:
            custom_repos = json.loads(args.custom_repo_names)
        custom_branches = {}
        if args.custom_branch_names:
            custom_branches = json.loads(args.custom_branch_names)

        ssh_client = coriolis_cd_utils._get_ssh_client(
            args.host, args.username, args.password)

        coriolis_build._run_coriolis_build(
            ssh_client=ssh_client,
            local_ssh_key_path=args.key_path,
            remote_ssh_key_path=args.remote_key_path,
            registry=args.registry_host,
            registry_username=args.registry_username,
            registry_password=args.registry_password,
            export_list=args.export_providers,
            import_list=args.import_providers,
            coriolis_docker_repository_url=args.coriolis_docker_repo_url,
            coriolis_docker_branch_name=args.coriolis_docker_branch_name,
            custom_repo_names=custom_repos,
            custom_branch_names=custom_branches)
