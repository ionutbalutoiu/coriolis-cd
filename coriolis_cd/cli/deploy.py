# Copyright 2019 Cloudbase Solutions Srl
# All Rights Reserved.

from cliff.command import Command

from coriolis_cd import utils as coriolis_cd_utils
from coriolis_cd.operations import coriolis_deploy


class Deploy(Command):
    """Deploy coriolis components on an existing VM via SSH"""

    def get_parser(self, prog_name):
        parser = super(Deploy, self).get_parser(prog_name)
        parser.add_argument('--host', dest="host", required=True,
                            help='Hostname/address of the VM')
        parser.add_argument('--username', dest="username", required=True,
                            help='VM username.')
        parser.add_argument('--password', dest="password",
                            help='VM password', required=True)
        parser.add_argument('--docker-registry', dest="registry_host",
                            help='Docker registry', required=True)
        parser.add_argument('--docker-registry-user', dest="registry_username",
                            help='Docker registry user', required=True)
        parser.add_argument('--docker-registry-password',
                            dest="registry_password",
                            help='Docker registry user', required=True)
        parser.add_argument('--docker-pull-images', action="store_true",
                            dest="docker_pull", help="Set this flag to pull "
                            "the upstream Docker images before starting "
                            "the containers")
        parser.add_argument('--coriolis-docker-repo-url',
                            dest="coriolis_docker_repo_url",
                            required=False, default="https://bitbucket.org/"
                            "cloudbase/coriolis-docker.git",
                            help='The URL used to clone coriolis-docker '
                            'git repository')
        parser.add_argument('--coriolis-docker-branch-name',
                            dest="coriolis_docker_branch_name",
                            required=False, default="master",
                            help='The branch name used to clone '
                            'coriolis-docker git repository')
        return parser

    def take_action(self, args):
        ssh_client = coriolis_cd_utils._get_ssh_client(
            args.host, args.username, args.password)

        coriolis_deploy._run_coriolis_deployment(
            ssh_client=ssh_client,
            registry=args.registry_host,
            registry_username=args.registry_username,
            registry_password=args.registry_password,
            pull_images=args.docker_pull,
            coriolis_docker_repository_url=args.coriolis_docker_repo_url,
            coriolis_docker_branch_name=args.coriolis_docker_branch_name)
