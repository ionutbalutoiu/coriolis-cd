# Copyright 2023
# Cloudbase Solutions Srl
# All Rights Reserved.

from cliff.show import ShowOne

from coriolis_cd.operations import coriolis_bm_network


class BaremetalMachineGetNetwork(ShowOne):
    """Get BM machine network for automated BareMetal tests"""

    def get_parser(self, prog_name):
        parser = super(BaremetalMachineGetNetwork, self).get_parser(prog_name)
        parser.add_argument('--id', dest="bm_id", required=True,
                            help='The ID of the BM machine.')
        parser.add_argument('--bm-endpoint', dest="bm_endpoint",
                            default="appliance-metal-hub",
                            type=str,
                            help='The metal Hub endpoint name or ID')
        parser.add_argument("--auth-url",
                            type=str,
                            help="Coriolis Keystone auth-url.",
                            required=True)
        parser.add_argument("--user-name",
                            type=str,
                            help="Coriolis Keystone username.",
                            required=True)
        parser.add_argument("--user-password",
                            type=str,
                            help="Coriolis Keystone user password.",
                            required=True)
        parser.add_argument("--project-name", default="admin",
                            type=str,
                            help="Coriolis Keystone project name.")
        parser.add_argument("--user-domain-name", default="Default",
                            type=str,
                            help="Coriolis Keystone user domain name.")
        parser.add_argument("--project-domain-name", default="Default",
                            type=str,
                            help="Coriolis Keystone project domain name.")
        return parser

    def take_action(self, args):

        columns, data = coriolis_bm_network.get_bm_network(
            args.bm_id, args.bm_endpoint, args.auth_url, args.user_name,
            args.user_password, args.project_name, args.user_domain_name,
            args.project_domain_name)

        return (columns, data)
