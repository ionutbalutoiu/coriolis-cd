# Copyright 2019 Cloudbase Solutions Srl
# All Rights Reserved.

import time

from coriolis_cd import constants
from coriolis_cd import utils
from coriolis_cd.testing.common import get_exception_details
from coriolis_cd.testing.migration.cleanup import get_cleanup_provider
from coriolis_cd.testing.migration.validation import get_validation_provider


class CoriolisTester(object):

    def __init__(self, coriolis_session):
        self._session = coriolis_session
        self._client = coriolis_session.get_client()

    def _wait_replica_execution(self, replica, execution):
        while True:
            # TODO(ibalutoiu): Workaround for not getting `TypeError: get()
            # takes exactly 3 arguments (2 given)` when calling `get()` first
            # time. The second call to `get()` works without any problem.
            try:
                execution = self._client.replica_executions.get(
                    replica, execution)
            except Exception:
                execution = self._client.replica_executions.get(
                    replica, execution)

            if execution.status in constants.ACTIVE_EXECUTION_STATUSES or (
                    execution.status == constants.EXECUTION_STATUS_UNEXECUTED):
                utils._log_msg(
                    "Replica execution is in status: %s" % execution.status)
                time.sleep(30)
            elif execution.status in constants.FINALIZED_EXECUTION_STATUSES:
                if execution.status == constants.EXECUTION_STATUS_COMPLETED:
                    utils._log_msg("Replica execution completed")
                    break
                else:
                    raise Exception(
                        "Replica execution in error state: %s. Please check "
                        "the Coriolis logs for more info." % execution.status)
            else:
                raise Exception(
                    "Replica execution is in invalid state: %s" % (
                        execution.status))
        return execution

    def _wait_migration(self, migration):
        while True:
            migration = self._client.migrations.get(migration.base_id)
            status = migration.last_execution_status

            if status in constants.ACTIVE_EXECUTION_STATUSES or (
                    status == constants.EXECUTION_STATUS_UNEXECUTED):
                utils._log_msg("Migration is in status: %s" % status)
                time.sleep(30)
            elif status in constants.FINALIZED_EXECUTION_STATUSES:
                if status == constants.EXECUTION_STATUS_COMPLETED:
                    utils._log_msg("Migration completed!")
                    break
                else:
                    raise Exception(
                        "Migration is in error state: %s. Please check the "
                        "Coriolis logs for more info." % status)
            else:
                raise Exception(
                    "Migration is in invalid state: %s" % status)

        return migration

    def _get_public_ip_address(self,
                               instance_result,
                               migration,
                               max_retries=10):
        ip_address_dest = instance_result.get("public_ip_address")
        i = 0
        vm_data = None
        vm_data_dict = {}
        while not ip_address_dest and i < max_retries:
            utils._log_msg("Trying to fetch IP address from "
                           "destination environment.\n")
            try:
                vm_data = self._client.endpoint_instances.get(
                    instance_id=instance_result.get("name"),
                    endpoint=migration.destination_endpoint_id)
                vm_data_dict = vm_data.to_dict()
            except Exception:
                utils._log_msg("Could not get VM details from destination "
                               "endpoint. Error was: %s\n" % (
                                   utils.get_exception_details()))
                raise

            first_nic = vm_data_dict['devices']['nics'][0]
            nic_ip_addresses = [
                ip for ip in first_nic.get('ip_addresses', [])
                if utils.validate_ipv4_address(ip)]

            if nic_ip_addresses:
                ip_address_dest = nic_ip_addresses[0]
                break
            utils._log_msg("Waiting for IP address to show up in "
                           "instance details.\n")
            i += 1
            time.sleep(30)

        utils._log_msg("IP address of migrated VM is: %s" % ip_address_dest)
        return ip_address_dest

    def create_replica(self,
                       origin_endpoint_name,
                       destination_endpoint_name,
                       source_environment={},
                       destination_environment={},
                       network_map={},
                       storage_mappings={},
                       instances=[]):
        origin_endpoint_id = self._client.endpoints.get_endpoint_id_for_name(
            endpoint=origin_endpoint_name)
        dest_endpoint_id = self._client.endpoints.get_endpoint_id_for_name(
            endpoint=destination_endpoint_name)

        return self._client.replicas.create(
            origin_endpoint_id=origin_endpoint_id,
            destination_endpoint_id=dest_endpoint_id,
            source_environment=source_environment,
            destination_environment=destination_environment,
            instances=instances,
            network_map=network_map,
            storage_mappings=storage_mappings,
            user_scripts={})

    def run_replica(self, replica):
        execution = self._client.replica_executions.create(replica)

        return self._wait_replica_execution(replica, execution)

    def run_migration_from_replica(self, replica_id):
        migration = self._client.migrations.create_from_replica(
            replica_id=replica_id, user_scripts={})

        return self._wait_migration(migration)

    def run_migration(self,
                      origin_endpoint_name,
                      destination_endpoint_name,
                      source_environment={},
                      destination_environment={},
                      network_map={},
                      storage_mappings={},
                      instances=[]):
        origin_endpoint_id = self._client.endpoints.get_endpoint_id_for_name(
            endpoint=origin_endpoint_name)
        dest_endpoint_id = self._client.endpoints.get_endpoint_id_for_name(
            endpoint=destination_endpoint_name)

        migration = self._client.migrations.create(
            origin_endpoint_id=origin_endpoint_id,
            destination_endpoint_id=dest_endpoint_id,
            source_environment=source_environment,
            destination_environment=destination_environment,
            instances=instances,
            network_map=network_map,
            storage_mappings=storage_mappings,
            user_scripts={})

        return self._wait_migration(migration)

    def cleanup_replica_disks(self, replica, fatal=True):
        try:
            execution = self._client.replicas.delete_disks(replica)
            return self._wait_replica_execution(replica, execution)
        except Exception:
            if fatal:
                raise
            utils._log_msg(
                "Failed to cleanup disks for replica '%s'.Exception "
                "details:\n%s" % (replica.id, get_exception_details()))

    def validate_migration(self, migration, ports=[]):
        if len(ports) == 0:
            utils._log_msg("The validation ports list is empty. Skipping")
            return
        transfer_result = migration.transfer_result.to_dict()
        for vm_name in migration.instances:
            instance_result = transfer_result[vm_name]
            public_ip_address = self._get_public_ip_address(
                instance_result=instance_result,
                migration=migration)
            validation_provider = get_validation_provider(public_ip_address)
            utils._log_msg("Validating migrated VM with IP address:"
                           "'%s'" % public_ip_address)
            validation_provider.validate(ports=ports)

    def cleanup_migration(self, migration, fatal=True):
        try:
            dest_endpoint = self._client.endpoints.get(
                migration.destination_endpoint_id)
            conn_info = self._session.get_secret_connection_info(
                dest_endpoint.connection_info.to_dict()
            )
            dest_env = migration.destination_environment.to_dict()
            transfer_result = migration.transfer_result.to_dict()
            for vm_name in migration.instances:
                instance_result = transfer_result[vm_name]
                cleanup_provider = get_cleanup_provider(
                    provider_type=dest_endpoint.type,
                    connection_info=conn_info,
                    destination_env=dest_env,
                    instance_result=instance_result)
                cleanup_provider.cleanup()
        except Exception:
            if fatal:
                raise
            utils._log_msg(
                "Failed to cleanup migration '%s'. Exception details:\n%s" % (
                    migration.id, get_exception_details()))
