# Copyright 2019 Cloudbase Solutions Srl
# All Rights Reserved.

import json

from coriolis_cd.testing import common


@common.retry_on_error()
def add_coriolis_endpoint(coriolis_client, name,
                          description, endpoint_type, connection_info):
    print("Adding Coriolis endpoint: %s" % (name))
    # Note: the regions parameter is required for the endpoints.create method
    regions = []
    for e in coriolis_client.endpoints.list():
        if name == e.name:
            print("The endpoint '%s' already exists" % (name))
            return e
    return coriolis_client.endpoints.create(
        name=name,
        description=description,
        endpoint_type=endpoint_type,
        connection_info=connection_info,
        regions=regions)


@common.retry_on_error()
def validate_coriolis_endpoint(coriolis_client, endpoint):
    print("Validating Coriolis endpoint: %s" % (endpoint.name))
    coriolis_client.endpoints.validate_connection(endpoint)


def add_and_validate_endpoint(auth_url, user_name, user_password, project_name,
                              user_domain_name, project_domain_name, endp_name,
                              description, endp_type, connection_info):
    coriolis_client = common.CoriolisSession(
        auth_url=auth_url,
        username=user_name,
        password=user_password,
        project_name=project_name,
        user_domain_name=user_domain_name,
        project_domain_name=project_domain_name).get_client()

    endpoint = add_coriolis_endpoint(
        coriolis_client,
        name=endp_name,
        description=description,
        endpoint_type=endp_type,
        connection_info=json.loads(connection_info))

    validate_coriolis_endpoint(coriolis_client, endpoint)
